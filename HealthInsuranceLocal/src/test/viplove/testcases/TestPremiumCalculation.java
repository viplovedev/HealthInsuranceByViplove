package test.viplove.testcases;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import test.viplove.business.PremiumCalculator;
import test.viplove.model.PremiumHolder;
import test.viplove.model.PremiumHolder.Gender;
import test.viplove.model.PremiumHolder.Habits;
import test.viplove.model.PremiumHolder.HealthConditions;

import java.util.ArrayList;

public class TestPremiumCalculation {

	@Test
	public void testCalculatePremium() {
		
		//Create profile for Norman Gomes
		String name = "Norman Gomes";
		Gender gender = Gender.MALE;
		int age = 34;
		
		ArrayList<HealthConditions> preExistingConditions = new ArrayList<HealthConditions>();
		preExistingConditions.add(HealthConditions.OVERWEIGHT); 
		
		ArrayList<Habits> habits = new ArrayList<Habits>();
		habits.add(Habits.ALCOHOL);
		habits.add(Habits.EXERCISE);
		
		//Calculate Premium
		PremiumHolder pH = new PremiumHolder(name, gender, age, preExistingConditions, habits);
		PremiumCalculator pC = new PremiumCalculator();
		
		float premium = pC.CalculatePremium(pH);
		assertEquals(6856, premium, 0.0);
	}
	
}
