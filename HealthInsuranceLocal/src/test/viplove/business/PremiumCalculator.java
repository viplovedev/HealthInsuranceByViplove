package test.viplove.business;

import java.util.List;
import test.viplove.model.PremiumHolder;
import test.viplove.model.PremiumHolder.HealthConditions;
import test.viplove.model.PremiumHolder.Habits;
import test.viplove.model.PremiumHolder.Gender;

public class PremiumCalculator {
	
	private static int basePremium = 5000;
	private float premium = basePremium;
		
	public float CalculatePremium(PremiumHolder pH)
	{
		System.out.println("Calculating premium for "+pH.getFullName());
		
		//Adjust premium on age
		AdjustPremiumOnAge(pH.getAge());
		
		//Adjust premium on gender
		AdjustPremiumOnGender(pH.getGender());
		
		//Adjust premium on pre-existing conditions
		AdjustPremiumOnExistingConditions(pH.getPreExistingConditions());
		
		//Adjust premium on habits
		AdjustPremiumOnHabits(pH.getHabits());
		
		return premium;
	}
	
	private void AdjustPremiumOnAge(int age) {
		
		System.out.println("\nAdjusting premium for age - " + age + "\n====================\nCurrent Premium:\t" + premium);
		
		if (age >= 18 ) {
			
			premium += premium*.1;
			
		}
		
		if (age >= 25 ) {
			
			premium += premium*.1;
		}
		
		if (age >= 30 ) {
			
			premium += premium*.1;
		}
		
		if (age >= 35 ) {
			
			premium += premium*.1;
		}
		
		if (age > 40 ) {
			
			int incr = (int) Math.floor((age - 40)/5) + 1;
			for (int i=1;i<=incr;i++) {
				premium = (float) (premium + premium*.2); 
			}
		}
		System.out.println("Adjusted premium:\t" + premium);
	}
	
	private void AdjustPremiumOnGender(Gender gender) {
		
		if (gender == Gender.MALE) {
			premium += premium*.02;
		}
	}
	
	private void AdjustPremiumOnExistingConditions(List<HealthConditions> healthConditions) {
		
		System.out.println("\nAdjusting premium for pre-existing conditions \n====================\nCurrent Premium:\t" + premium);
		healthConditions.forEach((i) -> {
			if(i == HealthConditions.BLOODPRESSURE || i == HealthConditions.BLOODSUGAR ||  i == HealthConditions.HYPERTENSION || i == HealthConditions.OVERWEIGHT) {
				
				premium += premium*.01;
				System.out.println("Increasing premium because of health condition - " + i);
				
			}
		});
		
		System.out.println("Adjusted Premium:\t" + premium);
	}
	
	private void AdjustPremiumOnHabits(List<Habits> habits) {
		
		System.out.println("\nAdjusting premium for habits \n====================\nCurrent Premium:\t" + premium);
		habits.forEach((i) -> {
			if(i == Habits.EXERCISE) {
				
				System.out.println("Reducing premium because of good habit - " + i);
				premium -= premium*.03;
			}
		});
		
		habits.forEach((i) -> {
			if(i == Habits.ALCOHOL || i == Habits.DRUGS ||  i == Habits.SMOKING) {
				
				System.out.println("Increasing premium because of bad habit - " + i);
				premium += premium*.03;
			}
		});
	}
}
