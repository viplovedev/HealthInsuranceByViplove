package test.viplove;

import java.util.ArrayList;

import test.viplove.business.PremiumCalculator;
import test.viplove.model.PremiumHolder;
import test.viplove.model.PremiumHolder.Gender;
import test.viplove.model.PremiumHolder.HealthConditions;
import test.viplove.model.PremiumHolder.Habits; 

public class HealthInsuranceClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("The Health Insurance Quote Calculator");
		System.out.println("=====================================");
		PremiumCalculator pc = new PremiumCalculator();
		
		//Create profile for Norman Gomes
		String name = "Norman Gomes";
		Gender gender = Gender.MALE;
		int age = 34;
		
		ArrayList<HealthConditions> preExistingConditions = new ArrayList<HealthConditions>();
		preExistingConditions.add(HealthConditions.OVERWEIGHT);
		
		ArrayList<Habits> habits = new ArrayList<Habits>();
		habits.add(Habits.ALCOHOL);
		//habits.add(Habits.DRUGS);
		//habits.add(Habits.SMOKING);
		habits.add(Habits.EXERCISE);
		
		//Calculate Premium
		PremiumHolder pH = new PremiumHolder(name, gender, age, preExistingConditions, habits);
		System.out.println(pH.getFullName());
		
		System.out.println("Final Premium:\t\t" + pc.CalculatePremium(pH));
	}

}