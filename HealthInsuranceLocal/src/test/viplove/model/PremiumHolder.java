package test.viplove.model;

import java.util.List;

public class PremiumHolder {
	
	
	private String fullName;
	private Gender gender;
	
	private int age;
	private List<HealthConditions> preExistingConditions;
	private List<Habits> habits;
	
	public PremiumHolder(String fullName, Gender gender, int age, List<HealthConditions> preExistingConditions, List<Habits> habits) {
		
		this.fullName = fullName;
		this.gender = gender;
		this.age = age;
		this.preExistingConditions = preExistingConditions;
		this.habits = habits;
		
	}
	
	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}
	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}
	/**
	 * @return the gender
	 */
	public Gender getGender() {
		return gender;
	}
	/**
	 * @return the preExistingConditions
	 */
	public List<HealthConditions> getPreExistingConditions() {
		return preExistingConditions;
	}
	/**
	 * @return the habits
	 */
	public List<Habits> getHabits() {
		return habits;
	}
	
	public enum Gender {
	    MALE, FEMALE
	}
	
	public enum HealthConditions {
		HYPERTENSION,BLOODPRESSURE,BLOODSUGAR,OVERWEIGHT
	}
	
	public enum Habits {
		SMOKING,ALCOHOL,DRUGS,EXERCISE
	}
	
}
